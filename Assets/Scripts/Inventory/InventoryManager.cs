using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

namespace DickyArya.Inventory
{
    public class InventoryManager : MonoBehaviour
    {
        [SerializeField] private GameObject _inventoryPanel;
        [SerializeField] private GameObject _content;

        [SerializeField] private int _maxItemHolder = 50;


        private bool _showInventory = false;
        private bool _showingInventoryInvoked = false;

        public bool ShowingInventoryInvoked => _showingInventoryInvoked;

        private void Start()
        {
            _inventoryPanel.SetActive(false);
        }

        private void Update()
        {
            if (_showInventory)
            {
                if (!_showingInventoryInvoked)
                    ShowInventory(true);
            }
            else
                ShowInventory(false);
        }

        private void ShowInventory(bool show)
        {
            _showingInventoryInvoked = show;
            _inventoryPanel.SetActive(show);
        }

        public void InventoryHandler()
        {
            _showInventory = !_showInventory;
        }


    }

}
